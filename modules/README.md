Embedded Pipeline Modules Folder
================================

This is the embedded pipeline modules folder. You can add modules in here, but you should generally keep your modules in
an external modules folder, since XNAT pipeline modules may eventually be added here.

